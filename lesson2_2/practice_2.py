# Symbol escaping
# Explain the error
#print("He said, "What's there?"")

# using triple quotes
print('''He said, "What's there?"''')

# escaping single quotes
print('He said, "What\'s there?"')

# escaping double quotes
print("He said, \"What's there?\"")

# Special characters to format the text
print("Hello my name Oybek, \nWhat's your name?")
print("My name is \tOtabek")

# The format() Method
text_a = "Your total payment is:"
numeric_val = 100.52

# focus on how to we avoid an error in concatenating with different Python types
result = "{} {}".format(text_a, numeric_val)
print(result)

# order using positional argument
level_1 = "Middle"
level_2 = "Junior"
level_3 = "Senior"

positional_order = "{1}, {0} and {2}".format(level_1, level_2, level_3)
print('\n--- Python expert levels ---')
print(positional_order)

# another way to format with "f"
val_1 = 3
val_2 = 0
print(f"Division {val_2} to {val_2} will cause a Python error")

# Old style formatting
location = "Planet Mars"
print('The value of location is %s' % location)

# basic python methods on string
# capitalize
city = 'tashkent'
country = 'Uzbekistan'

text = "The capital of {1} is {0}".format(city.capitalize(), country.capitalize())
print(text)

# lower
word_1 = "CAPITAL"
text = "The {0} of {2} is {1}".format(word_1.lower(), city.capitalize(), country.capitalize())
print(text)

# replace
city = 'London'
country = 'Russia'
text = "The capital of {} is {}".format(country, city.replace("London", "Moscow"))
print(text)

#strip
long_text = """    The story’s moral is simple — your wisdom can help you win wealth. """
print(long_text.strip())

monologue = """
- Say my name!
- What?
- I said say my name!!
"""
name = 'Heisenberg'

print(f"Your name's {name.upper()}!")

# title()
abbr = "USA"
full_text = "united states of america"
print("USA - " + full_text.title())


