# Math string formatting

# formatting integers
val_bin = "Binary representation of {0} is {0:b}".format(12)
print(val_bin)

# formatting floats
val_exp = "Exponent representation: {0:e}".format(1566.345)
print(val_exp)

# round off
val_round = "One third is: {0:.3f}".format(1/3)
print(val_round)
