
"""
Constructor. Program illustrates some instructions to build various models from input items.
The program provides four models you can build or construct: Vehicle, Air, House, Computer. You will be prompted to
select randomly provided list of items, you must select the items that you may build those listed models.
Program will check your inputs and provide most fitted models. There should be at least 3 items correctly chosen.
You can ask for another list of randomly selected items twice.
If nothing can be built you will be lost.
"""
import random

MODELS = {
    "Car": ["wheels", "engine", "gearbox", "wipers", "blinkers"],
    "Boat": ["paddle", "sail", "anchor", "compass", "keel"],
    "House": ["bricks", "concrete", "roof", "windows", "walls"],
    "Computer": ["motherboard", "cpu", "ssd", "mouse", "wi-fi"]
}


def randomizer():
    result = []
    full_list = []
    for k, v in MODELS.items():
        full_list += v

    counts = 5
    while counts > 0:
        rnd = random.choice(full_list)
        if rnd in result:
            rnd = random.choice(full_list)
        else:
            counts = counts - 1
        result.append(rnd)

    return result


def check_for_model(parts):
    models = {}
    result = "Nothing"
    for pt in parts:
        for k, v in MODELS.items():
            if pt in MODELS[k]:
                models[k] = models.get(k, 0) + 1

            if models.get(k, 0) >= 3:
                result = k

    return result


if __name__ == '__main__':
    selects = []
    print("You can build the following items using by provided parts: Car, Boat, House, Computer")
    print("You will be provided randomly generated list of parts, you must choose at least 3 parts")

    print("If you see there are not enough parts you can request another randomly generated list only twice")

    parts = "PASS"
    attempts = 3
    while parts.upper() == "PASS" and attempts > 0:
        print("Choose parts from the list:")
        print(randomizer())
        print("To request another list of parts write PASS")
        parts = input("Enter parts by comma separated or PASS")
        attempts -= 1

    res = check_for_model(parts.split(','))
    print(f"Congratulations! You constructed {res}!")
