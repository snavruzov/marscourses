print("Welcome to Rainbow Zoo, our animals always happy to see you!")
print("We have following animals here:")
animal1 = "Tiger"
animal1_weight = 90.6
animal1_age = 6
animal1_color = "Orange"
animal1_spec = "Cat"
animal1_name = "Matilda"

animal2 = "Lion"
animal2_weight = 100.5
animal2_age = 7
animal2_color = "Yellow"
animal2_spec = "Cat"
animal2_name = "Simba"

animal3 = "Grizzly"
animal3_weight = 210.5
animal3_age = 10
animal3_color = "Brown"
animal3_spec = "Bear"
animal3_name = "Bobby"

print(animal1, animal2, animal3)
print("Do you have an animal to donate our Zoo?")
print("Cool, enter its details:")

animal4 = input("What animal you have? ")
animal4_weight = float(input("How much it weighs? "))
animal4_age = int(input("How old is it? "))
animal4_color = input("What color is it? ")
animal4_spec = input("What species does it belong to ? ")
animal4_name = input("How do you call it? ")

print("Thank you for your donation")
print("Now our Zoo has:")
print(animal1, animal2, animal3, animal4)

print("Your animal details:")
print(animal4)
print("Weight: ", animal4_weight)
print("Age: ", animal4_age)
print("Color: ", animal4_color)
print("Species: ", animal4_spec)
print("Name: ", animal4_name)

