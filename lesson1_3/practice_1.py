# Before printing the result let students guess it
a = "1"
b = "2"
c = a + b
print("a+b=", c)

c_converted = int(a) + int(b)
print("a+b=", c_converted)

a_number = 5
b_number = 6

# Before printing the result let students guess it
c = str(a_number) + str(b_number)
print(c)

a_float = 1.6
b_float = 2.5

result_text = str(a_float) + str(b_float)
print(result_text)

a_float = "0.25"
b_float = "0.5"

result = a_float + b_float
print(result)

result = float(a_float) + float(b_float)
print(result)

# we can write expressions directly inside the print in Python
print(float('5.6'), int('3'), 1+3)

a_float = "2.5"
awesome_float = float(a_float)
best_result = int(awesome_float)
print(best_result)

# Try to investigate the error, let students find out
a_number = int("2.5")
print(a_number)

b_number = int(2.4)
print(b_number)


