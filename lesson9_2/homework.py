"""
Hello Languages. There is a Hello word translated in 5 languages. En, Ru, Es, Fr, Uz.
The program asks a user to input languages, separated by commas, that is familiar.
The program should print Hello in the languages entered by the user. Hint: use function with args, split input string
by comma and pass to the function. If there is no language in the list print default Hello in English.
"""

LANGS = ["En", "Ru", "Es", "Fr", "Uz"]


def translator(*args):
    translates = []
    for kv in args:
        kv = kv.strip()
        if kv == 'En':
            translates.append("Hello in English")
        elif kv == 'Ru':
            translates.append("Привет на Русском")
        elif kv == 'Es':
            translates.append("Hola es Hispaniola")
        elif kv == 'Fr':
            translates.append("Bonjour de France")
        elif kv == 'Uz':
            translates.append("Salom O'zbek tilida")
        else:
            translates.append("Hello in English by default")

    return translates


if __name__ == '__main__':
    print(f'There are following languages I know to speak {LANGS}')
    langs = input("Please choose languages that you want say Hello, multiple languages separated by commas")
    langs_selected = langs.split(',')

    translated = translator(*langs_selected)
    print(translated)

