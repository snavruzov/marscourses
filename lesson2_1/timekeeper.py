"""
TimeKeeper. Create a program that calculates the time you can get to from one city to another.
Use input statements to enter city names, distance between them(in kilometers) and the speed you are moving.
In result, it should print what time you reach your destination in hours.
"""

dest_a = input("Enter the departure city: ")
dest_b = input("Enter the arrival city: ")

distance = input("What is distance between these cities in kilometers? ")
speed = input("What is your car speed in km/h? ")
print("I hope your speed is NOT 0 km/h :)")

print("Computing the time you will reach the destination...")
time = float(distance) / int(speed)

print("You will reach", dest_b, "in", time, "hours")
