# Students try to find a Triangle Hypotenuse using Pythagoras formula
import math

print("Triangle Hypotenuse")
length = input("Enter the length:")
width = input("Enter the width:")

hypotenuse = math.sqrt(int(length) ** 2 + int(width) ** 2)
print("hypotenuse is equal to: ", hypotenuse)