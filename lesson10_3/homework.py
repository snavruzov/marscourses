
"""
Excel Demo. Program should implement a table look data using dictionary objects. Suppose there is
information about Students. The student has a name, age, year of study, subject and grades by each subject.
The program should be able to provide information about a student(name) by user input data and sorted by his/her grades.
Hints:
Store student information in dictionary, use at least 3 subjects per student.
Use sorted() function to sort student grades.
"""

STUDENTS = [
    {"name": "Alice Cooper", "age": 23, "yos": 2, "subjects": [{"name": "Pysics", "grade": 3},
                                                               {"name": "Math", "grade": 4},
                                                               {"name": "Biology", "grade": 2}]},
    {"name": "Bob Syncler", "age": 33, "yos": 3, "subjects": [{"name": "Pysics", "grade": 5},
                                                              {"name": "Math", "grade": 4},
                                                              {"name": "Biology", "grade": 3}]},
    {"name": "Tod Casper", "age": 22, "yos": 1, "subjects": [{"name": "Pysics", "grade": 5},
                                                             {"name": "Math", "grade": 4},
                                                             {"name": "Biology", "grade": 5}]}
]


def get_grade(dict_item):
    return dict_item['grade']


if __name__ == '__main__':
    selects = []
    name = input("Enter a student name: ")

    student_info = None
    for student in STUDENTS:
        if student['name'] == name:
            student_info = student

    if student_info:
        sorted_info = sorted(student_info['subjects'], key=get_grade)
        student_info['subjects'] = sorted_info
        print(student_info)
    else:
        print("Cannot find such student, try another name!")
