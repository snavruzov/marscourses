
"""
File finder. Program should find all txt files form a specified directory. Move them to another folder.
Open these files and print the first 100 symbols from there.
Hint:
Use listdir to find txt files using endswith() string methods.
Use shutils to copy multiple files to the specified folder.

"""
import os
import shutil


def get_text_from_file(file):
    with open(file) as f:
        txt = " ".join(f.readlines())

    print(txt[:100])


if __name__ == '__main__':
    dir_1 = "sample_folder"
    dir_2 = "next_folder"
    files = os.listdir(dir_1)
    for fl in files:
        if fl.endswith('.txt'):
            dest = shutil.copy(f"{dir_1}/{fl}", f"{dir_2}/{fl}")
            get_text_from_file(dest)
