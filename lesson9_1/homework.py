"""
Checkerboard. Write a program that prints the initial setup of a checkerboard, with a 1 where a piece would be and a 0
where a blank square would be
"""

BOARD_DATA = [[0, 1, 0, 1, 0, 1, 0, 1],
              [1, 0, 1, 0, 1, 0, 1, 0],
              [0, 1, 0, 1, 0, 1, 0, 1],
              [1, 0, 1, 0, 1, 0, 1, 0],
              [0, 1, 0, 1, 0, 1, 0, 1],
              [1, 0, 1, 0, 1, 0, 1, 0],
              [0, 1, 0, 1, 0, 1, 0, 1],
              [1, 0, 1, 0, 1, 0, 1, 0]]

PIECE = "[X]"
BLANK = "[ ]"

if __name__ == '__main__':
    print("This program prints out checkerboard on the window!")

    for row in BOARD_DATA:
        ROW = []
        for box in row:
            if box == 1:
                ROW.append(PIECE)
            else:
                ROW.append(BLANK)
        print(" ".join(ROW))


