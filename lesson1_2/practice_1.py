a = "1"
b = "2"
c = "a+b=3"
print(a, b, c)

a = "Hello"
b = "Mars"
space = " "

# Let student think about the result and explain string can be any character combination from ASCII
print(a, space, b)

a = 1
b = 2
c = "a+b=3"

# Working with float numbers
a_float = 0.5
b_float = 2.5

result = a_float + b_float
print(result)

a_number = 1
b_float = 1.8

result = a_number + b_float
print(result)

long_text = "Mars is the fourth planet from the Sun and the second-smallest " \
            "planet in the Solar System, being larger than only Mercury. " \
            "In the English language, Mars is named for the Roman god of war."

print(long_text)
