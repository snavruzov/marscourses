
"""
Lie To Me. Create a program with five input questions, answer can be True or False,
as results it should sum all your answers and print out some score you answered right.
Hint: 0 and 1 can be converted as False and True
"""

print("Hello stranger, you are standing here to get all my gold treasures")
print("But you must pass my Quiz, If you collect the best scores the treasures yours, get ready!")
print("I accept only 0 or 1: 0 is NO, 1 is YES")

score = 0
answer_one = input("The Pyramids are located in Egypt, it's YES or NO?: ")
score = score + int(bool(answer_one))

answer_two = input("Penguins can't fly, it's YES or NO?: ")
score = score + int(bool(answer_two))

answer_three = input("Python isn't a language created to speak with snakes, it's YES or NO?: ")
score = score + int(bool(answer_three))

answer_four = input("Mars IT school is the best, it's YES or NO?: ")
score = score + int(bool(answer_four))

answer_five = input("Elephants are the biggest animal in the world, it's YES or NO?: ")
score = score + int(bool(answer_five))

print(f"Your score is: {score}")
print(f"If your score is less than 5 you lost, and never get the treasures!")
