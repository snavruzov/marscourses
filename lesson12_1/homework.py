
"""
Email parser. There is a file that contains multiple email messages. The program should be able to
get all senders' email address. The email address follows the standard format From: <username@example.com>.
Hint:
Use string slicing and indexing methods to find all email patterns.
Use list type to store emails for further prints.
"""

sender_emails = []


def parse_email(text):
    if "From: <" in text:
        text = text.replace("From: ", "")
        sender_emails.append(text[text.index('<')+1: text.index(">")])


if __name__ == '__main__':

    with open("sample.txt") as f:
        lines = f.readlines()

    for ln in lines:
        parse_email(ln)

    print(sender_emails)
