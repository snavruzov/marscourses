"""
Exclamat!on Po!nts Ask the student for string words to enter 5 times
and then print the same words with every lowercase i replaced
with an exclamation point. Hint: store input words in the list, use for loop to find "i" symbol
"""

TEXT_DATA = []
TEXT_DATA_RESULT = []

if __name__ == '__main__':
    text = input("Enter the first word, there should be not less than 4 words in total.")
    TEXT_DATA.append(text)
    for i in range(4):
        text = input("Enter the next word.")
        TEXT_DATA.append(text)

    for txt in TEXT_DATA:
        if 'i' in txt:
            txt = txt.replace('i', '!')

        TEXT_DATA_RESULT.append(txt)

    print(TEXT_DATA_RESULT)


